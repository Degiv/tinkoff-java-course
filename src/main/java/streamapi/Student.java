package streamapi;

import lombok.Getter;

@Getter
public class Student {
    private final int identifier;
    private final String name;
    private final int age;

    public Student(int identifier, String name, int age) {
        this.identifier = identifier;
        this.name = name;
        this.age = age;
    }
}
