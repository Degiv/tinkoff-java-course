package streamapi;

import java.util.*;
import java.util.stream.Collectors;

public final class StudentFilter {

    private StudentFilter() {}

    public static int getAgeSum(List<Student> list, String name) {
        return list.stream()
                .filter(student -> student.getName()
                        .equalsIgnoreCase(name))
                .mapToInt(Student::getAge)
                .sum();
    }

    public static Set<String> getNameSet(List<Student> list) {
        return list.stream()
                .map(Student::getName)
                .collect(Collectors.toSet());
    }

    public static boolean containsElder(List<Student> list, int toCompare) {
        return list.stream()
                .map(Student::getAge)
                .anyMatch(age -> age > toCompare);
    }

    public static Map<Integer, String> getIdMap(List<Student> list) {
        return list.stream()
                .collect(Collectors.toMap(Student::getIdentifier, Student::getName, (existing, replacement) -> replacement));
    }

    public static Map<Integer, Collection<Student>> getAgeMap(List<Student> list) {
        return list.stream()
                .collect(Collectors.toMap(Student::getAge,
                pivotStudent -> list.stream()
                        .filter(student -> student.getAge() == pivotStudent.getAge())
                        .collect(Collectors.toList()), (existing, replacement) -> existing));
    }
}
