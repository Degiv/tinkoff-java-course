package streamapi;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StudentFilterTest {

    private final static List<Student> list = List.of(
            new Student(111, "John", 10),
            new Student(111, "Bob", 20),
            new Student(222, "Paul", 10),
            new Student(333, "John", 30),
            new Student(444, "Trevor", 40)
    );

    @Test
    public void getAgeSum() {
        int actual = StudentFilter.getAgeSum(list, "John");
        int expected = list.get(0).getAge() + list.get(3).getAge();
        assertEquals(expected, actual);
    }

    @Test
    public void getNameSet() {
        Set<String> actual = StudentFilter.getNameSet(list);
        Set<String> expected = new HashSet<>(Arrays.asList("John", "Bob", "Paul", "Trevor"));
        assertEquals(expected, actual);
    }

    @Test
    public void containsElder() {
        boolean actual = StudentFilter.containsElder(list,39);
        assertTrue(actual);
    }

    @Test
    public void getIdMap() {
        Map<Integer, String> actual = StudentFilter.getIdMap(list);
        Map<Integer, String> expected = new HashMap<>();
        expected.put(111, "Bob");
        expected.put(222, "Paul");
        expected.put(333, "John");
        expected.put(444, "Trevor");
        assertEquals(expected, actual);
    }

    @Test
    public void getAgeMap() {
        Map<Integer, Collection<Student>> actual = StudentFilter.getAgeMap(list);
        Map<Integer, List<Student>> expected = new HashMap<>();
        expected.put(10, List.of(list.get(0), list.get(2)));
        expected.put(20, List.of(list.get(1)));
        expected.put(30, List.of(list.get(3)));
        expected.put(40, List.of(list.get(4)));
        assertEquals(expected, actual);
    }
}
